# Bitbucket Pipelines Pipe:  b4a-deploy
Deploy a [Parse Server](https://parseplatform.org/) application to [Back4App](https://back4app.com)

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yml
- pipe:  docker://raschidjfr/b4a-deploy:latest
  variables:
    B4A_ACCOUNT_KEY: '<string>'
    B4A_APP_NAME: '<string>'
    CLOUD_PATH: '<string>'  # Don't forget to include package.json inside

    # PUBLIC_PATH: '<string>' # Optional
    # MESSAGE: '<string>' #Optional
    # DEBUG: '1' # Optional
```

## Variables

| Variable              | Description                                                                                                                                                                                                                                                                          |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `B4A_ACCOUNT_KEY` (*) | Your account key for Back4App. Get one [here](https://dashboard.back4app.com/classic#/wizard/◊account-key)                                                                                                                                                                           |
| `B4A_APP_NAME` (*)    | The name of your app as it appears on your Back4App dashboard (currently **no whitespace** support!). See [Create your first App at Back4App](https://www.back4app.com/docs/get-started/new-parse-app)                                                                               |
| `CLOUD_PATH` (*)      | Path to the folder containing `main.js` and `package.json`  to be deployed                                                                                                                                                                                                           |
| `PUBLIC_PATH`         | Path to a folder containing the files to be uploaded to the `public` folder on Back4App. Files in this folder will be served as a web hosting. See [How can I use Back4App webhosting?](https://help.back4app.com/hc/en-us/articles/360002120991-How-can-I-use-Back4App-webhosting-) |
| `MESSAGE`             | Optional message to attach as a description on the release. This will be shown by calling the B4A CLI: `b4a releases`                                                                                                                                                                |
(*) *Required*


## Details

This pipeline uses the [Back4app CLI](https://blog.back4app.com/cli-parse-server/) to upload Cloud Code to your B4A account. 


Please refer to the Back4App's Cloud Code [Getting Started](https://www.back4app.com/docs/get-started/cloud-functions) guide for details.

## Prerequisites

* You'll need an account on [Back4app.com](https://dashboard.back4app.com/)
* You will need to have at least one app hosted on Back4app

## Examples

```yml
script:
  - cp package.json build   # don't forget to add package.json!
  - pipe: bitbucketpipelines/b4a-deploy
    variables:
      B4A_ACCOUNT_KEY: $MY_B4A_ACCOUNT_KEY
      B4A_APP_NAME: 'My Cool App'
      CLOUD_PATH: 'build'
      PUBLIC_PATH: 'assets'
      DEBUG: '1'
```

## Support

For reporting bugs or feature requests, please use the repo's [Issue Tracker](https://bitbucket.org/raschidjfr/b4a-deploy/issues)
