const rewire = require('rewire');
const fs = require('fs');
const path = require('path');
const {pathToTestProject, MockSpawn} = require('../helpers');

describe('Deploy', () => {
  const pathToCloudProject = path.resolve(pathToTestProject, 'dist');
  const pathToPublicFiles = path.resolve(pathToTestProject, 'assets');
  let spawn;
  let spawnMock;
  let deploy;
  let runModule;

  beforeEach(() => {
    spawnMock = new MockSpawn();
    spawn = () => spawnMock;
    deploy = rewire('../../src/deploy');
    runModule = deploy.__with__({spawn});
  });

  it('copy public data', () => {
    runModule(async () => {
      deploy.__set__('deployToBack4App', () => Promise.resolve());
      await deploy(pathToCloudProject, {publicAssetDir: pathToPublicFiles});

      const originalFiles = fs.readdirSync(pathToPublicFiles);
      const copiedFiles = fs.readdirSync(path.join(pathToCloudProject, 'public'));
      expect(copiedFiles).toEqual(originalFiles);
    });
  });

  it('call b4a cli deploy', () => {
    spawnMock = jasmine.createSpy('spawn');
    runModule(async () => {
      deploy.__set__('copyPublicDataContent', () => Promise.resolve());

      await deploy(pathToCloudProject);
      expect(spawnMock).toHaveBeenCalledWith('b4a', 'deploy', {cwd: path.resolve(pathToCloudProject)});
    });
  });

  it('notify if main.js is not found', () => {
    spawnMock = jasmine.createSpy('spawn');
    runModule(async () => {
      await expectAsync(deploy(pathToTestProject)).toBeRejectedWith(new Error('main.js was not found in ' + pathToTestProject));
    });
  });
});
