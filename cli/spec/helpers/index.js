
const SpecReporter = require('jasmine-spec-reporter').SpecReporter;
const {MockSpawn} = require('../helpers/mocks');
const mkdirp = require('mkdirp');
const {ncp} = require('ncp');
const rimraf = require('rimraf');
const path = require('path');
const pathToTestProject = path.resolve('.', 'tmp');

jasmine.getEnv().clearReporters();
jasmine.getEnv().addReporter(
  new SpecReporter({
    spec: {displayPending: true},
  }),
);

beforeEach(async () => {
  await removeTmpDir();
  await createTmpProject();
});

afterEach(async () => {
  await removeTmpDir();
});

async function createTmpProject() {
  const originalTestProject = path.resolve('spec/helpers/testProject');
  await mkdirp(pathToTestProject);
  await new Promise((resolve, reject) => {
    ncp(originalTestProject, pathToTestProject, (err) => {
      if (err) reject(err);
      else resolve();
    });
  });
}

function removeTmpDir() {
  return new Promise((resolve, reject) => {
    rimraf(pathToTestProject, (err) => {
      if (err) reject(err);
      else resolve();
    });
  });
}

module.exports = {
  pathToTestProject,
  MockSpawn,
};
