## b4a-deploy CLI
Node.js wrapper for [Back4app CLI](https://blog.back4app.com/cli-parse-server/).

Run `node bin` from the root folder for help:

```
Usage: bin [options] [command]

Options:
  -V, --version                 output the version number
  -h, --help                    display help for command

Commands:
  setup [options] <directory>   setup a Back4App project
  deploy [options] <directory>  deploy a directory to Back4App
  package <subcommand>          Utility for preprocessing package.json
  help [command]                display help for command
```
