setup() {
  load 'helpers/bats-support/load'
  load 'helpers/bats-assert/load'
  load 'helpers/mocks/stub'

  # get the containing directory of this file
  # use $BATS_TEST_FILENAME instead of ${BASH_SOURCE[0]} or $0,
  # as those will point to the bats executable's location or the preprocessed file respectively
  DIR="$( cd "$( dirname "$BATS_TEST_FILENAME" )" >/dev/null 2>&1 && pwd )"
  # make executables in src/ visible to PATH
  PATH="$DIR/..:$PATH"

  # Needed env vars
  export B4A_ACCOUNT_KEY="my-key"
  export B4A_APP_NAME="MyApp"
  export CLOUD_PATH="test/assets/cloud"
  export PUBLIC_PATH="test/assets/public"

  stub cp "echo 'call to \`cp\`'"
  stub_b4a
  clear_temp_dir
}

clear_temp_dir() {
  rm -rf temp
  mkdir -p temp/cloud
  mkdir -p temp/public
}

teardown() {
  rm -rf temp
}

stub_b4a() {
  stub b4a \
    "configure accountkey : echo 'call to \`configure accountkey\`' " \
    "new : mkdir -p temp/cloud; mkdir -p temp/public" \
    "default : echo 'SomeDefaultApp' " \
    "add ${B4A_APP_NAME} : echo 'call to \`add ${B4A_APP_NAME}\`' " \
    "default ${B4A_APP_NAME} : echo 'call to \`default ${B4A_APP_NAME}\`'" \
    "deploy : echo 'b4a_deploy_called' >> test.log "
}

@test "can deploy" {
  entry.sh
  [ "$(cat temp/test.log | grep b4a_deploy_called)" ]
  unstub b4a
}

@test "can deploy when there's only one app in account" {
  unstub b4a | :
  stub b4a \
    "configure accountkey : echo 'call to \`configure accountkey\`' " \
    "new : mkdir -p temp/cloud; mkdir -p temp/public" \
    "default : echo 'Current default app is ${B4A_APP_NAME}' " \
    "deploy : echo 'b4a_deploy_called' >> test.log "

  entry.sh
  [ "$(cat temp/test.log | grep b4a_deploy_called)" ]
  unstub b4a
}

@test "copy correct file structure" {
  unstub cp | :
  CLOUD_PATH="test/assets/cloud"
  PUBLIC_PATH="test/assets/public"
  entry.sh

  CLOUD_FILES=$(ls $CLOUD_PATH)
  PUBLIC_FILES=$(ls $PUBLIC_PATH)
  [ "$CLOUD_FILES" == "main.js" ]
  [ "$PUBLIC_FILES" == "index.html" ]
}

@test "can deploy with custom message" {
  export MESSAGE='Hello world!'
  run entry.sh
  assert_output --partial 'done: Hello world!'
  [ "$status" -eq 0 ]
}

@test "fail if B4A_ACCOUNT_KEY is not provided" {
  B4A_ACCOUNT_KEY=""
  run entry.sh
  assert_output --partial 'Var B4A_ACCOUNT_KEY is missing!'
  [ "$status" -ne 0 ]
}

@test "fail if B4A_APP_NAME is not provided" {
  B4A_APP_NAME=""
  run entry.sh
  assert_output --partial 'Var B4A_APP_NAME is missing!'
  [ "$status" -ne 0 ]
}

@test "fail if CLOUD_PATH is not provided" {
  CLOUD_PATH=""
  run entry.sh
  assert_output --partial 'Var CLOUD_PATH is missing!'
  [ "$status" -ne 0 ]
}

@test "abort if b4a fails to set default app" {
  unstub b4a | :
  stub b4a \
    "configure accountkey : echo 'call to \`configure accountkey\`' " \
    "new : mkdir -p temp/cloud; mkdir -p temp/public" \
    "default : echo 'SomeDefaultApp' " \
    "add ${B4A_APP_NAME} : echo 'call to \`add\`' " \
    "default ${B4A_APP_NAME} : exit 123"

  run entry.sh
  [ "$status" -eq 123 ]
  unstub b4a
}
